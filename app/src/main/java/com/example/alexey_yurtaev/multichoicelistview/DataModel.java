package com.example.alexey_yurtaev.multichoicelistview;

public class DataModel {

    private String countryCode;
    private String countryName;
    private String version;
    private boolean checked;

    public DataModel(String countryCode, String countryName, String version, boolean checked) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.version = version;
        this.checked = checked;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return "DataModel{" +
                "countryCode='" + countryCode + '\'' +
                ", countryName='" + countryName + '\'' +
                ", version='" + version + '\'' +
                ", checked=" + checked +
                '}';
    }
}
