package com.example.alexey_yurtaev.multichoicelistview;

import java.util.Arrays;
import java.util.List;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView myList = (ListView) findViewById(R.id.list);
        Button getChoice = (Button) findViewById(R.id.getchoice);
        myList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        final List<DataModel> dataModels = Arrays.asList(
                new DataModel("rus", "Russia", "1", true),
                new DataModel("ukr", "Ukraine", "1", false));
        CountryListAdapter adapter = new CountryListAdapter(dataModels, this);
        myList.setOnItemClickListener(adapter);
        myList.setAdapter(adapter);
        getChoice.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selected = "";
                for (int i = 0; i < dataModels.size(); i++) {
                    DataModel dataModel = dataModels.get(i);
                    selected += (dataModel.isChecked() ? dataModel.getCountryCode() : "") + " ";
                }
                Toast.makeText(MainActivity.this,
                        selected,
                        Toast.LENGTH_LONG).show();
            }
        });
    }
}
