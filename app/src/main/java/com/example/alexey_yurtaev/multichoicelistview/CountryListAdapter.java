package com.example.alexey_yurtaev.multichoicelistview;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class CountryListAdapter extends ArrayAdapter<DataModel> implements AdapterView.OnItemClickListener {
    private List<DataModel> dataSet;
    private LayoutInflater lInflater;

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        dataSet.get(position).setChecked(!dataSet.get(position).isChecked());
        notifyDataSetChanged();
    }

    public CountryListAdapter(List<DataModel> data, Context context) {
        super(context, R.layout.list_item, data);
        this.dataSet = data;
        this.lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DataModel dataModel = getItem(position);
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.list_item, parent, false);
        }

        ((TextView) view.findViewById(R.id.country_name)).setText(dataModel.getCountryName());
        ((TextView) view.findViewById(R.id.country_version)).setText(dataModel.getVersion());
        ((TextView) view.findViewById(R.id.country_code)).setText(dataModel.getCountryCode());
        CheckBox selectedCheckBox = (CheckBox) view.findViewById(R.id.country_selected);
        selectedCheckBox.setChecked(dataModel.isChecked());
        selectedCheckBox.setTag(position);
        return view;
    }
}
